export async function emailById(ctx: Context, next: () => Promise<any>) {
	const {
		state: { emailId },
		clients: { emailById: emailClient },
	} = ctx

	const {
		headers,
		data,
		status: responseStatus,
	} = await emailClient.getEmailByIdWithHeaders(emailId)

	ctx.status = responseStatus
	ctx.body = data
	ctx.set('Cache-Control', headers['cache-control'])

	await next()
}
