import type { InstanceOptions, IOResponse } from '@vtex/api'
import { ExternalClient } from '@vtex/api'

export default class EmailById extends ExternalClient {
	constructor(ctx: any, options?: InstanceOptions) {
    const userId = ctx?.vtex?.params?.userId;
		super(
			`http://${ctx.account}.vtexcommercestable.com.br/api/dataentities/CL/search?userId=${userId}&_fields=email`,
			ctx,
			{
				...options,
				headers: {
					...options?.headers,
					...{ Accept: 'application/vnd.vtex.ds.v10+json' },
					...(ctx.adminUserAuthToken
						? { VtexIdclientAutCookie: ctx.adminUserAuthToken }
						: null),
					...(ctx.storeUserAuthToken
						? { VtexIdclientAutCookie: ctx.storeUserAuthToken }
						: null),
					VtexIdclientAutCookie: ctx.authToken,
					'X-Vtex-Use-Https': 'true',
				},
			}
		)
	}

	public async getEmailById(emailId: number): Promise<string> {
		return this.http.get(emailId.toString(), {
			metric: 'status-get',
		})
	}

	public async getEmailByIdWithHeaders(
		emailId: number
	): Promise<IOResponse<string>> {
		return this.http.getRaw(emailId.toString(), {
			metric: 'status-get-raw',
		})
	}
}
