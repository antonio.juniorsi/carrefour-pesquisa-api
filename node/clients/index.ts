import { IOClients } from '@vtex/api'

import EmailById from './emailById'

// Extend the default IOClients implementation with our own custom clients.
export class Clients extends IOClients {
  public get emailById() {
    return this.getOrSet('emailById', EmailById)
  }
}
